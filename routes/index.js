const express = require('express');
const controller = require('../controllers');

const router = express.Router();

/* GET home page. */
router.route('/')
  .get(controller.home);

// get machine form and submit machine form
router.route('/machine')
  .get(controller.getMachineForm)
  .post(controller.addMachine);

// get search page and search machines
router.route('/search')
  .get(controller.fetchMachineForm)
  .post(controller.fetchMachine);

module.exports = router;
