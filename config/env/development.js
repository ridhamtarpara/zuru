module.exports = {
  application: {
    env: 'development',
    hostName: 'localhost',
    httpPort: 3000,
  },
  mongodb: {
    url: 'mongodb://localhost:27017/zuru',
  },
};
