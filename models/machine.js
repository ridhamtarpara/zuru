const mongoose = require('mongoose');

const Schema = mongoose.Schema; //eslint-disable-line

const machineSchema = new Schema({
  name: { type: String, required: true, index: true },
  category: {
    _id: Schema.Types.ObjectId,
    name: String,
  },
  unitPerHour: Number,
  numberOfMachine: Number,
  costPerHour: Number,
  // static 8 but it can be configured
  maxWorkHours: { type: Number, default: 8 },
  // additional fields
  costPerUnit: Number,
  totalUnitPerHour: Number,
  isDeleted: { type: Boolean, default: false },
  deletedAt: { type: Date, default: null },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
});

// pre methods dont work with es6 arrow
machineSchema.pre('save', function nextFun(next) {
  // add cost per unit and total unit per hour in db at every save for fast operations on sort
  // as with pagination fetching all machine and then sorting won't work
  this.costPerUnit = this.costPerHour / this.unitPerHour;
  this.totalUnitPerHour = this.unitPerHour * this.numberOfMachine;
  next();
});

const machineModel = mongoose.model('machine', machineSchema);

module.exports = machineModel;
