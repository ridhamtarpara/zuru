const mongoose = require('mongoose');

const Schema = mongoose.Schema; //eslint-disable-line

const categorySchema = new Schema({
  name: { type: String, required: true, index: true },
  isDeleted: { type: Boolean, default: false },
  deletedAt: { type: Date, default: null },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
});

const categoryModel = mongoose.model('category', categorySchema);

module.exports = categoryModel;
