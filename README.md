# Zuru

> Time and Cost calculation project

## Getting Started
1. Clone the project repository.

```
    git clone https://gitlab.com/ridhamtarpara/zuru.git ./zuru
    cd zuru
    npm install
```

2. Install mongodb and node version if you don't have that

> You can use external mongodb service instead of localhost like mlab and give db url into `config/env/development.js`

3. run `npm run seed` to populate database
4. run `npm start`
5. Navigate to the browser.
    http://localhost:3000

## Improvements
- Use Pagination on getting machine list and fetch machines seperately for time and cost as with pagination fetching all and sortin on server won't work
- write test cases
- error and request logs (with winston or similar library)
- use build system like backpack or gulp
