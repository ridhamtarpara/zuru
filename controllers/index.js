const Category = require('../models/category');
const Machine = require('../models/machine');
const _ = require('lodash');

// function to calculate time needed to complete production
function fetchMinimunTime(machines, numberOfUnit = 100000) {
  return new Promise((resolve => {
    // add total hour needed for each machine
    let tMachine = machines.map(machine => {
      machine.hoursToComplete = numberOfUnit / machine.totalUnitPerHour;
      machine.daysToComplete = `${Math.floor(machine.hoursToComplete / machine.maxWorkHours)} Days ${machine.hoursToComplete % machine.maxWorkHours} Hours`;
      return machine;
    });
    // sort ascending on time needed
    tMachine = _.orderBy(tMachine, ['hoursToComplete'], ['asc']);
    resolve(tMachine);
  }));
}

// function to calculate cost to complete production
function fetchMinimumCost(machines, numberOfUnit = 100000) {
  return new Promise((resolve => {
    // get units or if not available then set 100000 as default
    // add cost for each machine
    console.log(typeof numberOfUnit);

    let cMachine = machines.map(machine => {
      machine.costToComplete = numberOfUnit * machine.costPerUnit;
      return machine;
    });
    // sort on cost
    cMachine = _.orderBy(cMachine, ['costToComplete'], ['asc']);
    resolve(cMachine);
  }));
}

module.exports = {
  // get home page, pass null machine to handle message for adding machine
  home: (req, res) => {
    res.render('index', { title: 'Express', machine: null });
  },

  // returns form with machine fields and category
  getMachineForm: (req, res) => {
    Category.find({})
      .then(categories => {
        res.render('machine', { categories });
      })
      .catch(e => {
        res.render('error', { error: e });
      });
  },

  // adds machine into db
  addMachine: (req, res) => {
    Category.find({ _id: req.body.category })
      .then(category => {
        req.body.category = category[0];
        return Machine.create(req.body);
      })
      .then(machine => {
        res.render('index', { title: 'Express', machine });
      })
      .catch(e => {
        res.render('error', { error: e });
      });
  },

  // returns machine search form
  fetchMachineForm: (req, res) => {
    Category.find({})
      .then(categories => {
        res.render('search', { categories, timeSortedMachines: null, costSortedMachine: null });
      })
      .catch(e => {
        res.render('error', { error: e });
      });
  },

  // get machines on base of query
  fetchMachine: (req, res) => {
    // search by category
    const query = {
      'category._id': req.body.category,
    };
    // if minimum arg is provided then add to query
    if (req.body.min) {
      query.unitPerHour = {
        $gte: req.body.min,
      };
    }
    // if maximum arg is provided then add to query
    if (req.body.max) {
      // check if object is already there
      if (query.unitPerHour) {
        query.unitPerHour.$lte = req.body.max;
      } else {
        query.unitPerHour = {
          $lte: req.body.max,
        };
      }
    }
    // find machines from db
    Machine.find(query)
    // calculate time and cost parallely and get all category for UI form pupose
      .then(machines => Promise.all([fetchMinimunTime(machines, req.body.units),
        fetchMinimumCost(machines, req.body.units),
        Category.find({})]))
      .then(([timeSortedMachines, costSortedMachine, categories]) => {
        res.render('search', { categories, timeSortedMachines, costSortedMachine });
      })
      .catch(e => {
        res.render('error', { error: e });
      });
  },
};
