/* eslint-disable */
// Database seeder used often in admin folder

const _ = require('lodash');


module.exports.dropDatabaseAndReseed = function seed (seedObject, models, mongoose, logger, cb) {
  mongoose.connection.db.dropDatabase(function (err) {
    if (err) throw err;
    console.log('db dropped');
    var i = _.values(seedObject)
             .reduce(function (sum, ary) {
                return sum + ary.length;
              }, 0);

    _.forOwn(seedObject, function (docList, modelName) {
      docList.forEach(function (doc) {
        const model = models[modelName];
        if (model === undefined) throw Error('Must provide model: ' + modelName);
        model.create(doc, function (err, res) {
          if (err) throw err;
          logger('Created', modelName);

          i--; if (i <= 0) cb();
        });
      });
    });
  });
};

module.exports.seedWithoutDeletion = function reseed (seedObject, models, mongoose, logger, cb) {
  var i = _.values(seedObject)
           .reduce(function (sum, ary) {
              return sum + ary.length;
            }, 0);

  _.forOwn(seedObject, function (docList, modelName) {
    docList.forEach(function (doc) {
      const model = models[modelName];
      if (model === undefined) throw Error('Must provide model: ' + modelName);
      model.create(doc, function (err, res) {
        if (err) throw err;
        logger('Created', modelName);

        i--; if (i <= 0) cb();
      });
    });
  });
};
