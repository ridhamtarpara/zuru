#!/usr/bin/env node
/* eslint-disable global-require, max-len */

// Original seed file. Creates nicely laid out data for today and tomorrow.
// Add your user account here if not already present, to test the app. (Simply
// create it in the Mongo console and then paste the data here.)

const Seeder = require('./seeder');
const mongoose = require('mongoose');
const config = require('../config');

const models = {
  category: require('../models/category'),
  machine: require('../models/machine'),
};

const seedObjects = {
  category: [
    {
      _id: '59f830c64084b168108a57d8',
      name: 'Molding machine',
    },
    {
      _id: '59f830c64084b168108a57d9',
      name: 'Turning machine',
    },
  ],

  machine: [
    {
      name: 'HXF',
      unitPerHour: 500,
      numberOfMachine: 10,
      costPerHour: 10000,
      category: {
        _id: '59f830c64084b168108a57d8',
        name: 'Molding machine',
      },
    },
    {
      name: 'HXM',
      unitPerHour: 200,
      numberOfMachine: 20,
      costPerHour: 3000,
      category: {
        _id: '59f830c64084b168108a57d8',
        name: 'Molding machine',
      },
    },
    {
      name: 'MXI',
      unitPerHour: 100,
      numberOfMachine: 30,
      costPerHour: 1000,
      category: {
        _id: '59f830c64084b168108a57d8',
        name: 'Molding machine',
      },
    },
    {
      name: 'CNC',
      unitPerHour: 800,
      numberOfMachine: 5,
      costPerHour: 750,
      category: {
        _id: '59f830c64084b168108a57d9',
        name: 'Turning machine',
      },
    },
    {
      name: 'VMC',
      unitPerHour: 400,
      numberOfMachine: 6,
      costPerHour: 300,
      category: {
        _id: '59f830c64084b168108a57d9',
        name: 'Turning machine',
      },
    },
  ],
};

const DEFAULT_DATABASE_URL = config.mongodb.url;

console.info(`Connecting to ${DEFAULT_DATABASE_URL}...`);
mongoose.connect(DEFAULT_DATABASE_URL, { useMongoClient: true });
mongoose.connection.on('open', connectionError => {
  if (connectionError) throw connectionError;

  Seeder.dropDatabaseAndReseed(seedObjects, models, mongoose, console.info, seedError => {
    if (seedError) throw seedError;
    console.info('\nDone!');
    process.exit(0);
  });
});
